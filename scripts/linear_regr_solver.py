import shelve

import numpy as np
from sklearn import linear_model


DATASET = np.genfromtxt('train.csv', delimiter=',', skip_header=True)
X = DATASET[:, 0:-1]
Y = DATASET[:, -1:]

MODEL = linear_model.LinearRegression().fit(X, Y)


def export_model():
    with shelve.open('model') as model:
        model['b0'] = MODEL.intercept_[0]
        model['B'] = MODEL.coef_[0]


if __name__ == '__main__':
    export_model()
