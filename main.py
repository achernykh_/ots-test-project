import shelve
from os import path

import numpy as np
from tornado.web import RequestHandler, Application
from tornado.ioloop import IOLoop

MODEL_SHELVE = shelve.open(path.join('scripts', 'model'))

assert 'B' in MODEL_SHELVE
assert 'b0' in MODEL_SHELVE


class BasicHandler(RequestHandler):
    @property
    def model_coef(self):
        return MODEL_SHELVE['B']

    @property
    def model_intercept(self):
        return MODEL_SHELVE['b0']

    def post(self):
        try:
            delimiter = self.request.body_arguments.get('delimiter', ',')[0]
            file = self.request.files.get('test')[0]
            body = file.get('body')
        except IndexError as ie:
            self.set_status(400)
            self.write("no file in request")
            return
        else:
            self.write({"Y_pred": list(self.solve_generator(body, delimiter=delimiter))})
        return

    def solve_generator(self, test_body: bytes, delimiter=b','):
        # get rid of header
        test_body = test_body[test_body.find(b'\n') + 1:]
        for row in test_body.splitlines():
            try:
                test_np_array = np.array(list(map(lambda x: np.float64(x), row.split(delimiter))))
            except ValueError as ve:
                yield np.NaN
            else:
                yield self.model_intercept + sum(test_np_array * self.model_coef)


class FormView(RequestHandler):
    def get(self):
        body = """
<html>
<head>
<title>test</title>
</head>
<body>
<form enctype="multipart/form-data" action="/api/files/regression/linear.csv" method="post">
<div><label>Файл:</label></div>
<div><input required id="test" type="file" name="test"></div>
<div><input required value="," id="delimiter" name="delimiter"></div>
<button>Отправить</button>
</form>
</body>
</html>
    """
        self.write(body)
        return


def make_app():
    return Application(
        [
            (r'^/api/files/regression/linear.csv$', BasicHandler),
            (r'^[/]?$', FormView),
        ],
        autoreload=True
    )


def run():
    app = make_app()
    app.listen(8080)
    print(f"Started Tornado server at localhost:8080")
    IOLoop.current().start()


if __name__ == '__main__':
    run()
